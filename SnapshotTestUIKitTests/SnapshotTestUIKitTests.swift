//
//  SnapshotTestUIKitTests.swift
//  SnapshotTestUIKitTests
//
//  Created by Anton Mikhailitsyn on 02.08.2023.
//

import XCTest
import SnapshotTesting
@testable import SnapshotTestUIKit

final class SnapshotTestUIKitTests: XCTestCase {
    
    var vc : UIViewController!

    override func setUp() {
        //isRecording = true
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        vc = storyboard.instantiateInitialViewController()
    }
    
    func testCase() {
        assertSnapshot(matching: vc, as: .image, named: "iPhone14")
        assertSnapshot(matching: vc, as: .image(on: .iPhoneSe), named: "iPhoneSe")
        assertSnapshot(matching: vc, as: .image(on: .iPhoneX), named: "iPhoneX")
        assertSnapshot(matching: vc, as: .image(on: .iPad10_2), named: "iPad10_2")
        assertSnapshot(matching: vc, as: .image(on: .iPhoneXsMax), named: "iPhoneXsMax")
    }
    
    override func tearDown() {
        super.tearDown()
        vc = nil
        
    }
}
